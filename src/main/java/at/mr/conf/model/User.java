package at.mr.conf.model;

import java.util.Set;

import org.springframework.data.annotation.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class User {

    @Id
    private String id;

    private String username;
    private String password;
    private String passwordConfirm;

    private Boolean expired;
    private Boolean locked;

    private Set<String> roles;

}
