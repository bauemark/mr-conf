package at.mr.conf.model;

import java.util.Date;

import lombok.Data;

@Data
public class Status {

    private Boolean active;

    private String current;
    private String next;
    private int currentIndex;
    private int numKlassenTotal;
    private Date startTime;
}
