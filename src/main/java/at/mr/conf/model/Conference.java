package at.mr.conf.model;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Conference {

    @Id
    private String conferenceId;
    private String title;

    private Date startTime;
    private Date endTime;

    private List<Klasse> klassen;
    private int current;
    private Date currentStartedAt;

    private Boolean active;

}
