package at.mr.conf.model;

import lombok.Data;

@Data
public class Action {
    private String action;
}
