package at.mr.conf.model;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Klasse {

    @Id
    private String name;

    private boolean selected;

    private int index;
}
