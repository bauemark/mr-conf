package at.mr.conf.model;

public class Role {

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_GOD = "ROLE_GOD";
    public static final String ROLE_USER = "ROLE_USER";

}
