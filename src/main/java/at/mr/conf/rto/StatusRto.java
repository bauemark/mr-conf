package at.mr.conf.rto;

import java.util.Date;

import lombok.Data;

@Data
public class StatusRto {

    private Boolean active;
    private String current;
    private String next;
    private Date startTime;
    private int currentIndex;
    private int numKlassenTotal;

}
