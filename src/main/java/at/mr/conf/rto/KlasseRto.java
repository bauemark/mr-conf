package at.mr.conf.rto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class KlasseRto {
    private String name;
    private int index;
}
