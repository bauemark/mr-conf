package at.mr.conf.rto;

import lombok.Data;

@Data
public class UserRto {

    private String id;

    private String username;
    private String password;
    private String passwordConfirm;

    private Boolean expired;
    private Boolean locked;

    private Boolean isAdmin;
    private Boolean isGod;

}
