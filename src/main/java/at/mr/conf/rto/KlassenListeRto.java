package at.mr.conf.rto;

import java.util.List;

import lombok.Data;

@Data
public class KlassenListeRto {

    private List<KlasseRto> selectedKlassen;
    private List<KlasseRto> unselectedKlassen;
}
