package at.mr.conf.rto;

import java.util.Date;

import lombok.Data;

@Data
public class ConferenceRto {
    private String conferenceId;
    private String title;
    private Date startTime;
}
