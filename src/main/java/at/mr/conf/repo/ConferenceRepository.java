package at.mr.conf.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import at.mr.conf.model.Conference;

public interface ConferenceRepository extends MongoRepository<Conference, String> {

}
