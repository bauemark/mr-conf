package at.mr.conf.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import at.mr.conf.model.User;

public interface UserRepository extends MongoRepository<User, String> {

	User findByUsername(String username);

}
