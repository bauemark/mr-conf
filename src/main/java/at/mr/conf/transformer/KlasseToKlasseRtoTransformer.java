package at.mr.conf.transformer;

import java.util.function.Function;

import at.mr.conf.model.Klasse;
import at.mr.conf.rto.KlasseRto;

public class KlasseToKlasseRtoTransformer implements Function<Klasse, KlasseRto> {

    @Override
    public KlasseRto apply(Klasse k) {
        if (k == null) {
            return null;
        }

        KlasseRto rto = new KlasseRto();
        rto.setIndex(k.getIndex());
        rto.setName(k.getName());

        return rto;
    }

}
