package at.mr.conf.transformer;

import java.util.function.Function;

import at.mr.conf.model.Klasse;
import at.mr.conf.rto.KlasseRto;

public class KlasseRtoToKlasseTransformer implements Function<KlasseRto, Klasse> {

    @Override
    public Klasse apply(KlasseRto rto) {
        if (rto == null) {
            return null;
        }

        Klasse k = new Klasse();
        k.setIndex(rto.getIndex());
        k.setName(rto.getName());

        return k;
    }

}
