package at.mr.conf.transformer;

import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import at.mr.conf.model.User;

public class UserToSpringUserTransformer
        implements Function<User, org.springframework.security.core.userdetails.UserDetails> {

    @Override
    public org.springframework.security.core.userdetails.UserDetails apply(User u) {
        return org.springframework.security.core.userdetails.User
                .withUsername(u.getUsername())
                .password(u.getPassword())
                .accountExpired(u.getExpired() == null ? false : u.getExpired())
                .accountLocked(u.getLocked() == null ? false : u.getLocked())
                .authorities(
                        u.getRoles().stream().map(r -> new SimpleGrantedAuthority(r)).collect(Collectors.toList()))
                .build();

    }

}
