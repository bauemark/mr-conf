package at.mr.conf.transformer;

import java.util.function.Function;

import at.mr.conf.model.Status;
import at.mr.conf.rto.StatusRto;

public class StatusToStatusRtoTransformer implements Function<Status, StatusRto> {

    @Override
    public StatusRto apply(Status status) {
        StatusRto rto = new StatusRto();

        rto.setCurrent(status.getCurrent());
        rto.setNext(status.getNext());
        rto.setCurrentIndex(status.getCurrentIndex());
        rto.setNumKlassenTotal(status.getNumKlassenTotal());
        rto.setActive(status.getActive());
        rto.setStartTime(status.getStartTime());
        return rto;
    }

}
