package at.mr.conf.transformer;

import java.util.function.Function;

import at.mr.conf.model.Role;
import at.mr.conf.model.User;
import at.mr.conf.rto.UserRto;

public class UserToUserRtoTransformer implements Function<User, UserRto> {

    @Override
    public UserRto apply(User u) {
        if (u == null) {
            return null;
        }
        UserRto rto = new UserRto();
        rto.setExpired(u.getExpired());
        rto.setId(u.getId());
        rto.setUsername(u.getUsername());
        rto.setLocked(u.getLocked());
        rto.setIsAdmin(u.getRoles().contains(Role.ROLE_ADMIN));
        rto.setIsGod(u.getRoles().contains(Role.ROLE_GOD));

        return rto;
    }

}
