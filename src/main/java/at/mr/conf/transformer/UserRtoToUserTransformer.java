package at.mr.conf.transformer;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import at.mr.conf.model.Role;
import at.mr.conf.model.User;
import at.mr.conf.rto.UserRto;

public class UserRtoToUserTransformer implements Function<UserRto, User> {

    @Override
    public User apply(UserRto rto) {
        if (rto == null) {
            return null;
        }
        User u = new User();
        u.setId(rto.getId());
        u.setExpired(rto.getExpired());
        u.setLocked(rto.getLocked());
        u.setPassword(rto.getPassword());
        u.setPasswordConfirm(rto.getPasswordConfirm());
        u.setUsername(rto.getUsername());

        Set<String> roles = new HashSet<>();
        if (rto.getIsGod() != null && rto.getIsGod())
            roles.add(Role.ROLE_GOD);

        if (rto.getIsAdmin() != null && rto.getIsAdmin())
            roles.add(Role.ROLE_ADMIN);

        roles.add(Role.ROLE_USER);

        u.setRoles(roles);

        return u;

    }

}
