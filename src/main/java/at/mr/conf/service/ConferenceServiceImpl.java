package at.mr.conf.service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import at.mr.conf.model.Conference;
import at.mr.conf.repo.ConferenceRepository;

@Service
public class ConferenceServiceImpl implements ConferenceService {

    private ConferenceRepository conferenceRepository;
    private Logger log = Logger.getLogger(this.getClass().getSimpleName());

    public ConferenceServiceImpl(ConferenceRepository conferenceRepository) {
        super();
        this.conferenceRepository = conferenceRepository;
    }

    @Override
    public List<Conference> getConferences() {

        return conferenceRepository.findAll();
    }

    @Override
    public Conference getCurrentConference() {
        // currently there is only ONE conference at all
        List<Conference> all = conferenceRepository.findAll();
        return all.stream().sorted(new Comparator<Conference>() {
            public int compare(Conference c1, Conference c2) {
                if (c1.getStartTime() == null)
                    return -1;
                if (c2.getStartTime() == null)
                    return 1;
                return c2.getStartTime().compareTo(c1.getStartTime());
            }

        }).collect(Collectors.toList()).get(0);
        // return conferenceRepository.findAll().get(0);
    }

    @Override
    public void nextKlasse() {
        Conference conference = getCurrentConference();
        if (conference.getCurrent() < conference.getKlassen().stream().filter(k -> k.isSelected()).count() - 1) {
            conference.setCurrent(conference.getCurrent() + 1);
            conference.setCurrentStartedAt(new Date());

        } else {
            conference.setActive(false);
        }

        conferenceRepository.save(conference);

    }

    @Override
    public void previousKlasse() {
        Conference conference = getCurrentConference();
        if (conference.getCurrent() > 0) {
            conference.setCurrent(conference.getCurrent() - 1);
            conference.setCurrentStartedAt(new Date());
            conference.setActive(true);
            conferenceRepository.save(conference);
        }
    }

    @Override
    public void resetConference() {
        Conference conf = getCurrentConference();
        conf.setCurrent(0);
        conf.setCurrentStartedAt(new Date());
        conf.setStartTime(new Date());
        conf.setActive(true);
        save(conf);
    }

    @Override
    public void restartCurrentKlasse() {
        Conference conf = getCurrentConference();
        conf.setCurrentStartedAt(new Date());
        save(conf);

    }

    @Override
    public void updateConference() {
        Conference conf = getCurrentConference();
    }

    @Override
    public Conference getConference(String conferenceId) {
        return conferenceRepository.findById(conferenceId).get();
    }

    @Override
    public void save(Conference conf) {
        conferenceRepository.save(conf);
    }

    @Override
    public Conference addConference(String title) {
        Conference c = new Conference();
        c.setCurrent(0);
        c.setActive(false);
        c.setCurrentStartedAt(null);
        c.setEndTime(null);
        c.setStartTime(null);
        c.setTitle(title);
        return conferenceRepository.save(c);
    }

    @Override
    public void startCurrentConference() {
        resetConference();
    }

    @Override
    public void endCurrentConference() {
        Conference c = getCurrentConference();
        c.setActive(false);
        c.setEndTime(new Date());
        save(c);

    }

}
