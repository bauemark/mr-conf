package at.mr.conf.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import at.mr.conf.model.Conference;
import at.mr.conf.model.Klasse;

@Service
public class KlassenServiceImpl implements KlassenService {

    private final ConferenceService conferenceService;

    private final Logger logger = LoggerFactory.getLogger(KlassenServiceImpl.class);

    public KlassenServiceImpl(ConferenceService conferenceService) {
        this.conferenceService = conferenceService;
        // init("1A");
        // init("1B");
        // init("2A");
        // init("2B");
    }

    // public Klasse getCurrent() {
    // return klassenRepository.findByName("1A");
    // }
    //
    // public Klasse getByName(String name) {
    // if (name == null) {
    // return null;
    // }
    // return klassenRepository.findByName(name);
    // }
    //
    // @Override
    // public Klasse addKlasse(String name) {
    // if (name == null) {
    // return null;
    // }
    //
    // if (!isValidName(name)) {
    // return null;
    // }
    //
    // Klasse k = klassenRepository.findByName(name);
    // if (k == null) {
    // k = new Klasse();
    // k.setName(name);
    // klassenRepository.save(k);
    // }
    // return k;
    // }

    // @Transactional(isolation = Isolation.SERIALIZABLE)
    // @Override
    // public synchronized void saveKlassen(List<String> klassenNames) {
    // klassenRepository.deleteAll();
    // klassenRepository.save(
    // klassenNames.stream()
    // .filter(name -> isValidName(name))
    // .distinct()
    // .map(name -> new Klasse(name))
    // .collect(Collectors.toList()));
    // }

    private boolean isValidName(String name) {
        if (name == null || name.isEmpty()) {
            return false;
        }
        final String allowedPattern = "[1-8][A-D](\\s)?(g|gym|wk|wiku|G|WK|WIKU|GYM)?";
        if (name.matches(allowedPattern)) {
            // logger.error("matched pattern: " + name);
            return true;
        }
        // logger.error("didnt match pattern: " + name);
        return false;
    }

    @Override
    public List<Klasse> getSelectedKlassen(String conferenceId) {
        Conference conf = conferenceService.getConference(conferenceId);

        if (conf == null) {
            return new ArrayList<>();
        }

        return conf.getKlassen().stream().filter(k -> k.isSelected()).collect(Collectors.toList());
    }

    @Override
    public List<Klasse> getUnselectedKlassen(String conferenceId) {
        Conference conf = conferenceService.getConference(conferenceId);

        if (conf == null) {
            return new ArrayList<>();
        }

        return conf.getKlassen().stream().filter(k -> !k.isSelected()).collect(Collectors.toList());
    }

    @Override
    public void saveKlassen(String conferenceId, List<Klasse> allKlassen) {
        Conference conf = conferenceService.getConference(conferenceId);

        List<Klasse> validKlassen = allKlassen.stream().filter(k -> this.isValidName(k.getName()))
                .collect(Collectors.toList());

        conf.setKlassen(validKlassen);

        conferenceService.save(conf);

    }

}
