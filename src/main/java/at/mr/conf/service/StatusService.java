package at.mr.conf.service;

import at.mr.conf.model.Status;

public interface StatusService {

    Status getStatus();

}
