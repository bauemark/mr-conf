package at.mr.conf.service;

import java.util.List;

import at.mr.conf.model.Conference;

public interface ConferenceService {

    public List<Conference> getConferences();

    public Conference getCurrentConference();

    public void nextKlasse();

    void previousKlasse();

    public void resetConference();

    public void restartCurrentKlasse();

    public void updateConference();

    public Conference getConference(String conferenceId);

    public void save(Conference conf);

    Conference addConference(String title);

    public void startCurrentConference();

    public void endCurrentConference();

}
