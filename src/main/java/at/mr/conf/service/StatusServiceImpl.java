package at.mr.conf.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.mr.conf.model.Conference;
import at.mr.conf.model.Klasse;
import at.mr.conf.model.Status;

@Service
public class StatusServiceImpl implements StatusService {

    private ConferenceService conferenceService;

    @Autowired
    public StatusServiceImpl(ConferenceService conferenceService) {
        this.conferenceService = conferenceService;
    }

    @Override
    public Status getStatus() {
        Status status = new Status();
        Conference conference = conferenceService.getCurrentConference();
        int currentIndex = conference.getCurrent();
        List<Klasse> selectedKlassen = getSelectedKlassen(conference);

        if (selectedKlassen.isEmpty() || currentIndex >= selectedKlassen.size()) {
            status.setCurrent("--");
            status.setNext("--");
        } else {
            status.setCurrent(selectedKlassen.get(currentIndex).getName());

            int nextIndex = currentIndex + 1;
            if (nextIndex >= selectedKlassen.size()) {
                status.setNext("Fertig!");
            } else {
                status.setNext(selectedKlassen.get(nextIndex).getName());
            }

        }

        status.setCurrentIndex(currentIndex + 1);
        status.setNumKlassenTotal(selectedKlassen.size());
        status.setActive(conference.getActive());
        status.setStartTime(conference.getCurrentStartedAt());

        return status;
    }

    private List<Klasse> getSelectedKlassen(Conference conference) {
        return conference.getKlassen().stream().filter(k -> k.isSelected())
                .collect(Collectors.toList());
    }
}
