package at.mr.conf.service;

import java.util.List;

import at.mr.conf.model.Klasse;

public interface KlassenService {

    List<Klasse> getSelectedKlassen(String conferenceId);

    List<Klasse> getUnselectedKlassen(String conferenceId);

    void saveKlassen(String conferenceId, List<Klasse> allKlassen);

    // Klasse getCurrent();
    //
    // Klasse getByName(String name);
    //
    // Klasse addKlasse(String name);
    //
    // void saveKlassen(List<String> klassenNames);
    //
    // List<Klasse> getAll();
}
