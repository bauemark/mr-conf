package at.mr.conf;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateUtil {

    public static String getFormattedTimeSince(Date then) {
        if (then == null) {
            return "not available";
        }

        long diff = System.currentTimeMillis() - then.getTime();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(diff) % 60;

        String formattedTime = "";
        if (minutes > 60) {
            formattedTime = String.format("%02d : ", minutes / 60);
        }
        return formattedTime + String.format("%02d : %02d", minutes % 60, seconds);

    }

}
