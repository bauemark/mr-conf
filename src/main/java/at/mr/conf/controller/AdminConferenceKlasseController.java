package at.mr.conf.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import at.mr.conf.model.Klasse;
import at.mr.conf.rto.KlasseRto;
import at.mr.conf.rto.KlassenListeRto;
import at.mr.conf.service.KlassenService;
import at.mr.conf.transformer.KlasseRtoToKlasseTransformer;
import at.mr.conf.transformer.KlasseToKlasseRtoTransformer;

@RestController
public class AdminConferenceKlasseController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final KlassenService klassenService;

    public AdminConferenceKlasseController(KlassenService klassenService) {
        this.klassenService = klassenService;
    }

    @GetMapping("/admin/conferences/{confId}/klassenliste")
    public ModelAndView klassenlisteNeu(@PathVariable String confId) {
        ModelAndView model = new ModelAndView("/klassenliste");
        model.addObject("confId", confId);
        return model;
    }

    @GetMapping(value = "/admin/conferences/{conferenceId}/klassen")
    public ResponseEntity<KlassenListeRto> getKlassenTable(HttpServletRequest request,
            @PathVariable String conferenceId) {

        List<Klasse> selected = klassenService.getSelectedKlassen(conferenceId);
        List<KlasseRto> selectedRtoList = selected.stream().map(new KlasseToKlasseRtoTransformer())
                .collect(Collectors.toList());

        List<Klasse> unselected = klassenService.getUnselectedKlassen(conferenceId);
        List<KlasseRto> unselectedRtoList = unselected.stream().map(new KlasseToKlasseRtoTransformer())
                .sorted((a, b) -> a.getName().compareTo(b.getName()))
                .collect(Collectors.toList());

        KlassenListeRto klassenListeRto = new KlassenListeRto();
        klassenListeRto.setSelectedKlassen(selectedRtoList);
        klassenListeRto.setUnselectedKlassen(unselectedRtoList);
        return ResponseEntity.ok(klassenListeRto);
    }

    @PostMapping(value = "/admin/conferences/{conferenceId}/klassen", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> postKlassenTable(@PathVariable String conferenceId,
            @RequestBody KlassenListeRto klassenListe) {

        List<KlasseRto> selectedRto = klassenListe.getSelectedKlassen();
        List<Klasse> selected = selectedRto.stream().map(new KlasseRtoToKlasseTransformer())
                .collect(Collectors.toList());
        selected.stream().forEach(k -> k.setSelected(true));

        List<KlasseRto> unselectedRto = klassenListe.getUnselectedKlassen();
        List<Klasse> unselected = unselectedRto.stream().map(new KlasseRtoToKlasseTransformer())
                .collect(Collectors.toList());
        unselected.stream().forEach(k -> k.setSelected(false));

        List<Klasse> allKlassen = new ArrayList<>();
        allKlassen.addAll(selected);
        allKlassen.addAll(unselected);

        klassenService.saveKlassen(conferenceId, allKlassen);

        return ResponseEntity.ok("");
    }

}