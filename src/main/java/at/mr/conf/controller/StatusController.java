package at.mr.conf.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import at.mr.conf.model.Status;
import at.mr.conf.rto.StatusRto;
import at.mr.conf.service.StatusService;
import at.mr.conf.transformer.StatusToStatusRtoTransformer;

@Controller
public class StatusController {

    private StatusService statusService;

    private final Logger log = LoggerFactory.getLogger(StatusController.class);

    @Autowired
    public StatusController(StatusService statusService) {
        this.statusService = statusService;
    }

    @GetMapping("/status")
    public ResponseEntity<StatusRto> getStatus() {
        log.debug("got /status");

        Status status = statusService.getStatus();
        StatusRto response = new StatusToStatusRtoTransformer().apply(status);

        return ResponseEntity.ok(response);
    }

    @GetMapping({ "/", "/confstatus" })
    public String root() {
        return "confstatus";
    }
}
