package at.mr.conf.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import at.mr.conf.service.ConferenceService;

@RestController
public class AdminController {

    private final ConferenceService conferenceService;

    public AdminController(ConferenceService conferenceService) {
        this.conferenceService = conferenceService;
    }

    @RequestMapping("/admin")
    public ModelAndView admin() {
        ModelAndView model = new ModelAndView("admin");
        model.addObject("conferenceId", conferenceService.getCurrentConference().getConferenceId());
        return model;
    }
}
