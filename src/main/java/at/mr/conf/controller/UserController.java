package at.mr.conf.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import at.mr.conf.auth.UserService;
import at.mr.conf.auth.UserValidator;
import at.mr.conf.model.User;
import at.mr.conf.rto.UserRto;
import at.mr.conf.transformer.UserRtoToUserTransformer;
import at.mr.conf.transformer.UserToUserRtoTransformer;

@Controller
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    private UserService userService;

    private UserValidator userValidator;

    public UserController(UserService userService, UserValidator userValidator) {
        this.userService = userService;
        this.userValidator = userValidator;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        logger.error("actually logged out");
        HttpSession session = request.getSession(false);
        SecurityContextHolder.clearContext();
        session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        for (Cookie cookie : request.getCookies()) {
            cookie.setMaxAge(0);
        }

        return "/";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        logger.error("got /login");
        if (error != null) {
            model.addAttribute("error", "Your username and password is invalid.");
            logger.error("Got error: " + error.toString());
        }
        if (logout != null) {
            model.addAttribute("message", "You have been logged out successfully.");
            logger.error("Got logout:");
        }
        return "login";
    }

    @RequestMapping(value = "/admin/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String userverwaltung(Model model) {
        return "users";
    }

    @GetMapping(value = "/users")
    public ResponseEntity<List<UserRto>> getUsers() {
        List<User> users = userService.getAllUsers();

        List<UserRto> userRtos = users.stream().map(new UserToUserRtoTransformer()).collect(Collectors.toList());
        return ResponseEntity.ok(userRtos);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<String> registration(@RequestBody UserRto userRto, BindingResult bindingResult, Model model) {
        User user = new UserRtoToUserTransformer().apply(userRto);
        user.setId(null);

        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            logger.error("Cannot create user because of validation errors");
            return ResponseEntity.badRequest().build();
        }

        userService.save(user);
        logger.error("User created");

        return ResponseEntity.ok("");
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteUser(@PathVariable String userId) {
        logger.error("soll user " + userId + " löschen");
        try {
            userService.deleteUser(userId);
            return ResponseEntity.ok("{ \"message\" : \"User wurde gelöscht\" }");

        } catch (Exception e) {
            String error = "{ \"message\" : \"" + e.getMessage() + "\" }";
            return ResponseEntity.badRequest().body(error);
        }

    }

    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public ResponseEntity<String> updateUserList(@RequestBody List<UserRto> users) {
        logger.error("updating user list");
        try {
            userService.updateUserList(users.stream().map(new UserRtoToUserTransformer()).collect(Collectors.toList()));
            return ResponseEntity.ok("{ \"message\" : \"Userliste wurde gespeichert\" }");

        } catch (Exception e) {
            String error = "{ \"message\" : \"" + e.getMessage() + "\" }";
            return ResponseEntity.badRequest().body(error);
        }

    }

}
