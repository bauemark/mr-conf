package at.mr.conf.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import at.mr.conf.model.Action;
import at.mr.conf.service.ConferenceService;

@RestController
public class AdminConferenceController {

    private final ConferenceService conferenceService;

    public AdminConferenceController(ConferenceService conferenceService) {
        this.conferenceService = conferenceService;
    }

    @PostMapping(value = "/admin/conference/{conferenceId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String postConferenceAction(@RequestBody Action action, @PathVariable String conferenceId) {
        switch (action.getAction()) {
        case "next":
            conferenceService.nextKlasse();
            break;

        case "previous":
            conferenceService.previousKlasse();
            break;

        case "reset":
            conferenceService.resetConference();
            break;

        case "resetCurrent":
            conferenceService.restartCurrentKlasse();
            break;

        case "start":
            conferenceService.startCurrentConference();
            break;

        case "end":
            conferenceService.endCurrentConference();
            break;
        }

        return "/admin";
    }

}
