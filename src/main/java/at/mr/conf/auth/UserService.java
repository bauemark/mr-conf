package at.mr.conf.auth;

import java.util.List;

import at.mr.conf.model.User;

public interface UserService {

    public void save(User user);

    public User getByUsername(String username);

    public List<User> getAllUsers();

    public void deleteUser(String userId);

    public void updateUserList(List<User> users);
}
