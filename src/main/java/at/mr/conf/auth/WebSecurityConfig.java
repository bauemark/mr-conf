package at.mr.conf.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

import at.mr.conf.model.Role;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private UserDetailsService userDetailsService;

    private AccessDeniedHandler accessDeniedHandler;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public WebSecurityConfig(UserDetailsService userDetailsService, AccessDeniedHandler accessDeniedHandler,
            BCryptPasswordEncoder bCryptPasswordEncoder) {
        super();
        this.userDetailsService = userDetailsService;
        this.accessDeniedHandler = accessDeniedHandler;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/js/public/**", "/webjars/**", "/status", "/status/**", "/ajax", "/welcome",
                        "/confstatus", "/css/**", "/registration", "/logout", "/media", "/impressum")
                .permitAll()
                .antMatchers("/admin", "/admin/conference/**", "/js/admin/**").hasAnyAuthority(Role.ROLE_ADMIN)
                .antMatchers("/users/**", "/admin/users", "/js/god/**").hasAuthority(Role.ROLE_GOD)
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login").successForwardUrl("/admin")
                .permitAll()
                // .and()
                // .logout()
                // .permitAll()
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler);

        // http.logout().logoutRequestMatcher(new
        // AntPathRequestMatcher("/logout"))
        // .clearAuthentication(true)
        // .invalidateHttpSession(true);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);

        // auth.inMemoryAuthentication()
        // .withUser("user").password("password").roles("USER")
        // .and()
        // .withUser("admin").password("admin").roles("ADMIN");
    }

}
