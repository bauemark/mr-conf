package at.mr.conf.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import at.mr.conf.model.User;
import at.mr.conf.transformer.UserToSpringUserTransformer;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private final UserService userService;

    public UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.error("loading User");

        User u = userService.getByUsername(username);

        if (u == null) {
            throw new UsernameNotFoundException(String.format("User %s not found", username));
        }

        // User user = new User();
        // user.setPassword(bCryptPasswordEncoder.encode("admin"));
        // user.setUsername("admin");
        // Set<Role> roles = new HashSet<Role>();
        // roles.add(Role.ROLE_ADMIN);
        // user.setRoles(roles);

        return new UserToSpringUserTransformer().apply(u);

        // User user = userRepository.findByUsername(username);
        //
        // Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        // for (Role role : user.getRoles()) {
        // grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        // }
        //
        // return new
        // org.springframework.security.core.userdetails.User(user.getUsername(),
        // user.getPassword(),
        // grantedAuthorities);

    }

}
