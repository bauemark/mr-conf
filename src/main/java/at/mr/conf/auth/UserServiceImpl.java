package at.mr.conf.auth;

import java.util.List;

import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import at.mr.conf.model.Role;
import at.mr.conf.model.User;
import at.mr.conf.repo.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setPasswordConfirm(null);
        userRepository.save(user);

    }

    @Override
    public User getByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void deleteUser(String userId) {
        User user = userRepository.findById(userId).get();

        if (user == null) {
            throw new RuntimeException("User existiert nicht");
        } else {
            if (user.getRoles().contains(Role.ROLE_GOD)) {
                throw new RuntimeException("User mit Rolle God kann nicht gelöscht werden");
            }
            userRepository.delete(user);
        }

    }

    @Override
    public void updateUserList(List<User> users) {
        for (User u : users) {
            User existingU = userRepository.findByUsername(u.getUsername());
            if (existingU == null) {
                throw new NotFoundException(String.format("User %s nicht gefunden", u.getUsername()));
            }
            existingU.setExpired(u.getExpired());
            existingU.setLocked(u.getLocked());
            existingU.setRoles(u.getRoles());
            userRepository.save(existingU);
        }

    }

}
