var changeSound;
var enableSound = true;
var receivedStatus = {};
var timeout = 1000;

$(function() {
	changeSound = new sound("/media/change.mp3");
});

(function check() {
	updateStatus();
	updateView();
}());

function updateStatus() {
	var that = this;
	$.get('/status').done(function(data) {
		that.receivedStatus = data;
	}).always(function() {
		setTimeout(updateStatus, timeout);
	});
}

function updateView() {
	var data = receivedStatus;
	if (data.active) {
		//console.log("active");
		$('#inactive').hide()
		$('#active').show()
		var current = $('#current-class').html();
		if (current != "" && current != data.current && enableSound) {
			changeSound.play();
		}
		$('#current-class').html(data.current);
		$('#next-class').html(data.next);
		
		var start = new Date(data.startTime);
		var end = new Date(Date.now());
		var elapsed = new Date(end - start - 3600000);
		var elapsedString = elapsed.getHours() + " : " + elapsed.getMinutes() + " : " + elapsed.getSeconds();
		$('#elapsed-time').html(elapsedString);

		$('#progress').html(data.currentIndex + " / " + data.numKlassenTotal);
		$('#progress').css({
			'width' : (data.currentIndex * 100 / data.numKlassenTotal) + '%'
		});
		
	} else {
		//console.log("inactive");
		$('#inactive').show()
		$('#active').hide()
	}
	
	setTimeout(updateView, 200);
}

function sound(src) {
	this.sound = document.createElement("audio");
	this.sound.src = src;
	this.sound.setAttribute("preload", "auto");
	this.sound.setAttribute("controls", "none");
	this.sound.style.display = "none";
	document.body.appendChild(this.sound);
	this.play = function() {
		this.sound.play();
	}
	this.stop = function() {
		this.sound.pause();
	}
}
