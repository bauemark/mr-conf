//var conferenceId = "";

function register() {
	clearMessages();

	$.ajax("/users/", {
		data : JSON.stringify({
			'username' : $("#username").val(),
			'password' : $("#password").val(),
			'passwordConfirm' : $("#password2").val(),
			'isAdmin' : $("#admin").prop('checked'),
			'isGod' : $("#god").prop('checked')
		}),
		contentType : 'application/json',
		type : 'POST'
	}).done(function(data) {
		message("Erfolg!", "Der User wurde angelegt.", "success");
	}).fail(function(data) {
		message("Fehler!", "Der User konnte nicht angelegt werden.", "danger");
	}).always(function(data) {
		$("#username").val("");
		$("#password").val("");
		$("#password2").val("");
	});

};

function clearMessages() {
	$('.alert').remove();
}

function message(headline, message, type) {
	var msg = [];
	msg.push("<div class='alert alert-" + type + "'>");
	msg.push("<strong>" + headline + "</strong> " + message);
	msg
			.push("<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>");
	msg.push("</div>");

	$('#row-message').append(msg.join(''));
}
