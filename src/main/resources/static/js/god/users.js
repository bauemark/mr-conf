//var conferenceId = "";

(function check() {
	loadTable();
}());


function checkboxString(isChecked) {
	var checked = '<input type="checkbox" checked/>';
	var unchecked = '<input type="checkbox" />';
	
	if (isChecked) {
		return '<td>' + checked + '</td>';
	} else {
		return '<td>' + unchecked + '</td>';
	}
}

function deleteUser(id) {
	$.ajax("/users/" + id + "/", {
		contentType : 'application/json',
		type : 'DELETE'
	}).done(function(data) {
		message("Gelöscht!", "User wurde gelöscht", "success");
	}).fail(function(data) {
		var msg = data.responseJSON.message;
		message("Nicht Gelöscht!", msg, "danger");
	}).always(function(data) {
		loadTable();
	});
}

function message(headline, message, type) {
	$('.alert').remove();
	
	var msg = [];
	msg.push("<div class='alert alert-" + type + "'>");
	msg.push("<strong>" + headline + "</strong> " + message);
	msg.push("<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>");
	msg.push("</div>");

	$('#row-message').append(msg.join(''));
}

function saveUsers() {
	var users = collectUserListAsJson();

	$.ajax("/users", {
		data : users,
		contentType : 'application/json',
		type : 'PUT'
	}).fail(function(data) {
		message("Fehler!", data.message, "danger");
	}).done(function(data) {
		message("Gespeichert!", "Die Daten wurden gespeichert", "success");
	}).always(function(data) {
		loadTable()
	});
	
}

function collectUserListAsJson() {
	var users = [];
	
	var list = $('#users tbody');
	var useritems = $('tr', list);
	
	
	$.each($("#users tbody tr"), function(i, item) {
//	$.each(useritems, function(i, item) {
		var user = {};
		var userfields = $(item).find('td');
		user.username = $(userfields[0]).text();
		user.isGod = $("input", userfields[1]).is(":checked");
		user.isAdmin = $("input", userfields[2]).is(":checked");
		user.locked = $("input", userfields[3]).is(":checked");
		user.expired = $("input", userfields[4]).is(":checked");
		users.push(user);
	});

	return JSON.stringify(users);
}

function loadTable() {
	$.get('/users')
	.done(function(data) {
		var html = "";
		for(var i = 0; i < data.length; i++) {
			html += '<tr>';
			html += '<td>' + data[i].username + '</td>';
			html += checkboxString(data[i].isGod);
			html += checkboxString(data[i].isAdmin);
			html += checkboxString(data[i].locked);
			html += checkboxString(data[i].expired);
			html += "<td><button class='btn btn-danger' onClick='deleteUser(\"" + data[i].id + "\")'>delete</button></td>";
			html += '</tr>';
		}
		            
		$('#users tbody tr').remove();
		$('#users tbody').html(html);
	});
}