//var conferenceId = "";
//
$(document).ready(function() {
	$.ajax("/status", {
		type : 'GET',
		success : (function(data) {
			if (data.active) {
				buttonsWhenActive()
				
			} else {
				buttonsWhenInactive()
			}
		})
	});
	
});

var enableSound = false;

function nextKlasse() {
	postConferenceAction('next');
};

function previousKlasse() {
	postConferenceAction('previous');
};

function reset() {
	postConferenceAction('reset');
};

function resetCurrent() {
	postConferenceAction('resetCurrent');
};

function endConference() {
	postConferenceAction('end')
	buttonsWhenInactive()
}

function startConference() {
	postConferenceAction('start')
	buttonsWhenActive()
}

function buttonsWhenActive() {
	$("#startConf").addClass("disabled");
	$("#endConf").removeClass("disabled");
	$("#btn-resetConf").removeClass("disabled");
	$("#btn-previous").removeClass("disabled");
	$("#btn-next").removeClass("disabled");
	$("#btn-resetCurrent").removeClass("disabled");
}

function buttonsWhenInactive() {
	$("#startConf").removeClass("disabled");
	$("#endConf").addClass("disabled");
	$("#btn-previous").addClass("disabled");
	$("#btn-next").addClass("disabled");
	$("#btn-resetCurrent").addClass("disabled");
	$("#btn-resetConf").addClass("disabled");
}

function postConferenceAction(action) {
	$.ajax("/admin/conference/" + conferenceId + "/", {
		data : JSON.stringify({
			'action' : action
		}),
		contentType : 'application/json',
		type : 'POST'
	});
};

function sortAsc(listId) {
	sort('asc', '#' + listId);

}

function sortDesc(listId) {
	sort('desc', '#' + listId);
}

function sort(direction, listId) {
	var sortableList = $(listId);
	var listitems = $('li', sortableList);

	listitems.sort(function(a, b) {
		if (direction == 'asc') {
			return ($(a).text().toUpperCase() > $(b).text().toUpperCase()) ? 1
					: -1;
		} else {
			return ($(a).text().toUpperCase() > $(b).text().toUpperCase()) ? -1
					: 1;
		}

	});
	sortableList.append(listitems);
}

function saveKlassenliste() {
	clearMessages();

	var selectedKlassen = collectKlassenFromUnorderedListAsJson();

	$.ajax("./klassen", {
		data : selectedKlassen,
		contentType : 'application/json',
		type : 'POST'
	}).done(
			function(data) {
				populateLists();

				message("Gespeichert!",
						"Die Klassen wurden erfolgreich gespeichert.",
						"success");
			});
}

function message(headline, message, type) {
	var msg = [];
	msg.push("<div class='alert alert-" + type + "'>");
	msg.push("<strong>" + headline + "</strong> " + message);
	msg
			.push("<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>");
	msg.push("</div>");

	$('#row-message').append(msg.join(''));
}

function collectKlassenFromUnorderedListAsJson() {
	var klassenListe = {};
	klassenListe.selectedKlassen = [];
	klassenListe.unselectedKlassen = [];

	var list = $('#selectedKlassen');
	var listitems = $('li', list);

	$.each(listitems, function(i, item) {
		var klasse = {};
		klasse.name = $(item).text();
		klasse.index = i;
		klassenListe.selectedKlassen.push(klasse);
	});

	var list = $('#unselectedKlassen');
	var listitems = $('li', list);

	$.each(listitems, function(i, item) {
		var klasse = {};
		klasse.name = $(item).text();
		klasse.index = i;
		klassenListe.unselectedKlassen.push(klasse);
	});
	return JSON.stringify(klassenListe);
}

function addNewKlasse() {
	var name = $('#newKlasse').val();
	if (name != "") {
		if (exists(name)) {
			message(
					"Achtung!",
					"Diese Klasse existiert bereits und wurde daher nicht hinzugefügt.",
					"danger");
		} else {
			$('#unselectedKlassen').append(newListItem(name));
		}
	}
	$('#newKlasse').val("");
}

function exists(klasse) {
	if ($('#unselectedKlassen li').filter(function() {
		return $(this).text() == klasse;
	}).length > 0) {
		return true;
	}
	if ($('#selectedKlassen li').filter(function() {
		return $(this).text() == klasse;
	}).length > 0) {
		return true;
	}
	return false;
}

function deleteFromList() {
	$('ul .active').remove();
	// var listitems = $('.selected', list);
	//
	// $.each(listitems, function(i, item) {
	// var klasse = {};
	// klasse.name = $(item).text();
	// klasse.index = i;
	// klassenListe.selectedKlassen.push(klasse);
	// });
}

function clearMessages() {
	$('.alert').remove();
}

function color(klasse) {
	switch (klasse.trim().charAt(0)) {
	case "1":
		return "#90AAF5";
	case "2":
		return "#90D2F5";
	case "3":
		return "#90EEF5";
	case "4":
		return "#90F5CB";
	case "5":
		return "#90F599";
	case "6":
		return "#D0F590";
	case "7":
		return "#E4F590";
	case "8":
		return "#F5DD90";
	default:
		return "white";
	}
}

function newListItem(text) {
	return '<li class="list-group-item" style="background-color: '
			+ color(text) + '">' + text + '</li>';
}

function shiftAllFromTo(from, to) {
	
	var addList = [];
	
	var existingList = $('#' + from);
	var listitems = $('li', existingList);

	$.each(listitems, function(i, item) {
		addList.push(newListItem($(item).text()));
		item.remove();
	});
	
	$('#' + to).append(addList.join(''));

}